/**

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   version 2 as published by the Free Software Foundation.

 *******************************

  After first time flashing, push the reboot button ( fix a bootloader known issue )

*/

#include <FS.h>      // this needs to be first, or it all crashes and burns...
#include <advancedSerial.h>
#include <ArduinoJson.h>
//#include <Arduino.h>
#include <ESP8266WiFi.h>
//#include <ESP8266WiFiMulti.h>
//#if CLIENT_SECURE == 1
//#include <WiFiClientSecure.h>
//#endif
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#if NTP_SERVER == 1
#include <WiFiUdp.h>
#endif
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include <WiFiManager.h>
#include <TimeLib.h>
#include <Ticker.h>
#include <Bounce2.h>

#include "config.h"
#include <MySensors.h>


#if OTA_SECURE == 1
#include <time.h>
#include <CertStoreBearSSL.h>
BearSSL::CertStore certStore;

class SPIFFSCertStoreFile : public BearSSL::CertStoreFile {
  public:
    SPIFFSCertStoreFile(const char *name) {
      _name = name;
    };
    virtual ~SPIFFSCertStoreFile() override {};

    // The main API
    virtual bool open(bool write = false) override {
      _file = SPIFFS.open(_name, write ? "w" : "r");
      return _file;
    }
    virtual bool seek(size_t absolute_pos) override {
      return _file.seek(absolute_pos, SeekSet);
    }
    virtual ssize_t read(void *dest, size_t bytes) override {
      return _file.readBytes((char*)dest, bytes);
    }
    virtual ssize_t write(void *dest, size_t bytes) override {
      return _file.write((uint8_t*)dest, bytes);
    }
    virtual void close() override {
      _file.close();
    }

  private:
    File _file;
    const char *_name;
};

SPIFFSCertStoreFile certs_idx("/certs.idx");
SPIFFSCertStoreFile certs_ar("/certs.ar");
#endif

#if NTP_SERVER == 1
WiFiUDP Udp;
#endif
//  ESP8266WiFiMulti WiFiMulti;
Bounce debouncer = Bounce();
Ticker ticker;
Config config;
WiFiManager wifiManager;

void checkSerial();
void loadConfig(const String filename, Config &config);
void saveConfig(const String filename, Config &config);
void initDefaultConfig(const String filename, Config &config);
void tick();
void setPins();
void checkButton();
void setReboot();
void setDefault();
void getDeviceId(Config &config);
void updateFile(const String fileName, int value);
void getUpdated(int which, const char* url);
void setPinsRebootUart();
void connectWifi();

// SETTINGS
WiFiManagerParameter customMqttServer("server", "mqtt server", config.mqttServer, sizeof(config.mqttServer));
WiFiManagerParameter customMqttPort("port", "mqtt port", config.mqttPort, sizeof(config.mqttPort));
WiFiManagerParameter customMqttUser("user", "mqtt user", config.mqttUser, sizeof(config.mqttUser));
WiFiManagerParameter customMqttPassword("password", "mqtt password", config.mqttPassword, sizeof(config.mqttPassword));
void quitConfigMode();
void saveConfigCallback();
void configModeCallback (WiFiManager *myWiFiManager);
void initConfigManager(Config &config);
void configManager(Config &config);

#if WEB_SERVER == 1
void handleNotFound();
#endif
#if NTP_SERVER == 1
time_t getNtpTime();
time_t prevDisplay = 0; // when the digital clock was displayed
void digitalClockDisplay();
void printDigits(int digits);
void sendNTPpacket(IPAddress &address);
#endif


void before() {
  Serial.begin(MY_BAUD_RATE);
#if DEBUG != 0
  aSerial.setPrinter(Serial);
#elif DEBUG == 0
  aSerial.off();
  Serial.setDebugOutput(false);
#endif
#if DEBUG == 1
  aSerial.setFilter(Level::v);
#elif DEBUG == 2
  aSerial.setFilter(Level::vv);
#elif DEBUG == 3
  aSerial.setFilter(Level::vvv);
#elif DEBUG == 4
  aSerial.setFilter(Level::vvvv);
#endif
  aSerial.v().p(F("====== ")).p(SKETCH_NAME).pln(F(" ======"));
  aSerial.v().println(F("====== Before setup ======"));

  for (uint8_t t = 4; t > 0; t--) { // Utile en cas d'OTA ?
    aSerial.vvv().print(F("[SETUP] WAIT ")).print(t).println(" ...");
    Serial.flush();
    delay(1000);
  }
  setPins();
  ticker.attach(1.5, tick);
  if (wifiResetConfig) { // add a button ?
    WiFiManager wifiManager;
    wifiManager.resetSettings();
  }
  if (resetConfig) {
    setDefault();
  }
  randomSeed(micros());
  getDeviceId(config);

  aSerial.vvv().print(F("before heap size : ")).println( ESP.getFreeHeap());
  aSerial.v().println(F("====== Setup started ======"));

  aSerial.vvv().pln(F("mounting FS..."));
  if (SPIFFS.begin()) {
    aSerial.vvv().pln(F("FS mounted"));
    if (SPIFFS.exists(configFileName)) {
      loadConfig(configFileName, config);
    } else {
      initDefaultConfig(configFileName, config);
    }
  } else {
    aSerial.vv().pln(F("Failed to mount FS."));
  }
#if MANUAL_SIGNING
  signPubKey = new BearSSL::PublicKey(pubkey);
  hash = new BearSSL::HashSHA256();
  sign = new BearSSL::SigningVerifier(signPubKey);
#endif

  connectWifi();

  initConfigManager(config);

  //  delay(1000);
#if NTP_SERVER == 1
  aSerial.vvv().println(F("Starting UDP"));
  Udp.begin(localPort);
  aSerial.vvv().print(F("Local port : ")).println(Udp.localPort());
  aSerial.vvvv().println(F("Waiting for sync "));
  setSyncProvider(getNtpTime);
  setSyncInterval(300);
  delay(100);
#endif

}

///////////////////////////////////////////////////////////////////////////////////
//    function to modifiy, save, settings for connection to wifi and server      //
///////////////////////////////////////////////////////////////////////////////////

void saveConfigCallback () {
  shouldSaveConfig = true;
}

void quitConfigMode() {
  if ( configMode == 0 ) {
    return;
  }
  else if ( configMode == 1 ) {
    wifiManager.setTimeout(5);
    detachInterrupt(digitalPinToInterrupt(MY_INCLUSION_MODE_BUTTON_PIN));
    aSerial.vv().pln(F("Quit config mode"));
    return;
  }
}

void configModeCallback (WiFiManager *myWiFiManager) {
  aSerial.v().pln(F("====== Config mode opening ======"));
  //aSerial.vvv().p(F("Portal SSID : ")).pln(myWiFiManager->getConfigPortalSSID());
  attachInterrupt(digitalPinToInterrupt(MY_INCLUSION_MODE_BUTTON_PIN), quitConfigMode, RISING);
  configMode = 1;
  wifiFailCount = 0;
  mqttFailCount = 0;
  ticker.attach(1, tick);
}

void initConfigManager(Config &config) {
#if DEBUG >= 3
  wifiManager.setDebugOutput(true);
#endif
#if DEBUG == 0
  wifiManager.setDebugOutput(false);
#endif
  wifiManager.setAPStaticIPConfig(IPAddress(192, 168, 244, 1), IPAddress(192, 168, 244, 1), IPAddress(255, 255, 255, 0));
  wifiManager.addParameter(&customMqttServer);
  wifiManager.addParameter(&customMqttPort);
  wifiManager.addParameter(&customMqttUser);
  wifiManager.addParameter(&customMqttPassword);
  //  IPAddress _ip, _gw, _sn;
  //  _ip.fromString(config.staticIp);
  //  _gw.fromString(config.staticGw);
  //  _sn.fromString(config.staticSn);
  //  wifiManager.setSTAStaticIPConfig(_ip, _gw, _sn);
}

void configManager(Config &config) {
  wifiManager.setAPCallback(configModeCallback);
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  //  wifiManager.setBreakAfterConfig(true);
  wifiManager.setMinimumSignalQuality(10);

  String script;
  // script += "<style>html{filter: invert(100%); -webkit-filter: invert(100%);}</style>"
  script += "<script>";
  script += "document.addEventListener('DOMContentLoaded', function() {";
  script +=     "var params = window.location.search.substring(1).split('&');";
  script +=     "for (var param of params) {";
  script +=         "param = param.split('=');";
  script +=         "try {";
  script +=             "document.getElementById( param[0] ).value = param[1];";
  script +=         "} catch (e) {";
  script +=             "console.log('WARNING param', param[0], 'not found in page');";
  script +=         "}";
  script +=     "}";
  script += "});";
  script += "</script>";
  wifiManager.setCustomHeadElement(script.c_str());

  configCount++;

  // After first start, hard reset, or without known WiFi AP
//  if (WiFi.status() != WL_CONNECTED) {
//    aSerial.vv().pln(F("Auto config access"));
//    if (!wifiManager.autoConnect(config.devEui, devicePass)) {
//      aSerial.v().pln(F("Connection failure --> Timeout"));
//      delay(3000);
//      setReboot();
//    }
//  }

  // When manually asking ...
  if ((configCount > 0 && manualConfig == true)) {
    //manualConfig = false;
    aSerial.vv().pln(F("Manual config access"));
    wifiManager.setTimeout(configTimeout * 2);
    wifiManager.startConfigPortal(config.devEui, config.devicePass);
  }
  // When wifi is already connected but connection got interrupted ...
  else if (WiFi.status() != WL_CONNECTED || !_MQTT_client.connected() || (strcmp(config.mqttServer, "") == 0) || (configCount > 0 && manualConfig == false)) {
    aSerial.vv().pln(F("User config access"));
    wifiManager.setTimeout(configTimeout);
    wifiManager.startConfigPortal(config.devEui, config.devicePass);
  }

  detachInterrupt(digitalPinToInterrupt(MY_INCLUSION_MODE_BUTTON_PIN));
  ticker.detach();
  digitalWrite(STATE_LED, HIGH);
  manualConfig = false;
  configMode = 0;

  if ( shouldSaveConfig ) {
    if ( (strcmp(customMqttServer.getValue(), "") != 0) ) {
      strlcpy(config.mqttServer, customMqttServer.getValue(), sizeof(config.mqttServer));
    }
    if ( (strcmp(customMqttPort.getValue(), "") != 0) ) {
      strlcpy(config.mqttPort, customMqttPort.getValue(), sizeof(config.mqttPort));
    }
    if ( (strcmp(customMqttUser.getValue(), "") != 0) ) {
      strlcpy(config.mqttUser, customMqttUser.getValue(), sizeof(config.mqttUser));
    }
    if ( (strcmp(customMqttPassword.getValue(), "") != 0) ) {
      strlcpy(config.mqttPassword, customMqttPassword.getValue(), sizeof(config.mqttPassword));
    }
    saveConfig(configFileName, config);
  }

  aSerial.v().pln();
  aSerial.vvv().pln(F("Config successful")).p(F("Config mode counter :")).pln(configCount);
  if (STATE_LED == LOW); digitalWrite(STATE_LED, HIGH);
  aSerial.vvv().print(F("config heap size : ")).println(ESP.getFreeHeap());
  aSerial.vv().p(F("IP Address : ")).pln(WiFi.localIP());
  aSerial.v().pln(F("====== Config ended ======"));

}

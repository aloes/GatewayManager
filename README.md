# GATEWAY MQTT

## Requirements

Arduino IDE - download the latest from arduino

- https://www.arduino.cc/en/Main/Software

Packages for ESP8266 development on Arduino IDE

- http://arduino.esp8266.com/stable/package_esp8266com_index.json

following libraries are required :

- ArduinoJson
- Bounce2
- FS
- MySensors
- Ticker
- WifiManager

## Installation

```
git clone https://github/courget/GatewayMQTT
```

Use MySensors librarie fork 
```
git clone https://github/courget/MySensors
```

Then in `config.h` file you may edit your configuration:

- Protect the acces point
```
char ap_pass[30]="yourpassword",
```

## Usage

- Open any .ino file of the folder with Arduino IDE
- Edit your preferences
- Set resetConfig to true, to make FS format and wifiManager resetting
- Upload the code on your ESP8266 board

Topic structure: MY_MQTT_PUBLISH_TOPIC_PREFIX/NODE-ID/SENSOR-ID/CMD-TYPE/ACK-FLAG/SUB-TYPE

## Dev

Go to the dev branch for the latest and unstable development
